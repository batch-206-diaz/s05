public class Person implements Actions,Greetings{

    public void sleep(){
        System.out.println("Zzzzz...");
    }

    public void run() {
        System.out.println("Running on the road!");
    }

    public void morningGreet(){
        System.out.println("Good Morning, Friend!");
    }

    public void holidayGreet(){
        System.out.println("Happy Holidays, Friend!");
    }

}

/*
    Interface
        - hides away the actual implementation of methods and simply shows a list/menu of available methods
        - 'implements' keyword is used in a class to use an interface
        - "blueprints" for classes where any class that implements the interface MUST have the methods in the interface
*/
