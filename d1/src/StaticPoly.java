public class StaticPoly {

    public int addition(int a, int b){
        return a+b;
    }

    //changing number of arguments
    public int addition(int a, int b, int c){
        return a+b+c;
    }

    //changing the type of arguments
    public double addition(double a, double b){
        return a+b;
    }
}

/*
    Static Polymorphism - achieved by overloading
	    - Overloading
	        - by changing the number of arguments
	        - by changing the type of arguments
*/