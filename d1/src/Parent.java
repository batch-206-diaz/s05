public class Parent {

    private String name;
    private int age;

    public Parent() {
    }

    public Parent(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public void greet(){
        System.out.println("Hello!");
    }
    public void greet(String name,String timeOfDay){
        System.out.println("Good " + timeOfDay + ", " + name + "!");
    }

    public void introduce(){
        System.out.println("Hi! I'm " + this.name + ". I am a parent.");
    }
}

/*
    Polymorphism
	    - is the ability of an object to take on many forms
	    - several objects may have the same function names but perform different functionalities based on their use
	    - achieved by creating a method of the same name but with different number of arguments

	    - Static Polymorphism
	    - Runtime Polymorphism or Dynamic Method Dispatch
	        - ability of subclass to inherit a method from a parent but overrides it and change its definition in the subclass

*/
