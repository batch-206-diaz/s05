public interface Actions {

    public void sleep();
    public void run();

}

/*
    Abstraction
        - process where all the logic and complexity are hidden from the user
        - the user would know what to do rather than how it is done
        - is achieved through the use of interfaces, "middleman"

    Interface
        - hides away the actual implementation of methods and simply shows a list/menu of available methods
        - 'implements' keyword is used in a class to use an interface
        - "blueprints" for classes where any class that implements the interface MUST have the methods in the interface

*/