public class Child extends Parent{

    public Child() {
    }

    public Child(String name, int age) {
        super(name, age);
    }

    //overrides the "introduce()" from Parent
    public void introduce(){
        System.out.println("I am a child!");
    }
}
