import java.awt.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public class Phonebook {

    ArrayList<Contact> contacts = new ArrayList<Contact>();

    public Phonebook() {
    }

    public Phonebook(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(Contact contacts) {
        this.contacts.add(0,contacts);
    }


    public void open(){
        int i = 0;
        if(contacts.isEmpty()){
            System.out.println("The phonebook is currently empty.");
        } else {
            while( i < contacts.toArray().length){
                System.out.println(contacts.get(i).getName() + " has the following registered number: " + contacts.get(i).getContactNumber());
                System.out.println(contacts.get(i).getName() + " has the following registered address: " + contacts.get(i).getAddress());
                i++;
            }
        }

    }
}
