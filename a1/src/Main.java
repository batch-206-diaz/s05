public class Main {
    public static void main(String[] args) {
        //System.out.println("Hello world!");


        Contact contact1 = new Contact("John Doe","+639152468596","Quezon City");
        Contact contact2 = new Contact("Jane Doe","+639162148573","Caloocan City");

        Phonebook phonebook = new Phonebook();

        phonebook.setContacts(contact1);
        phonebook.setContacts(contact2);

        phonebook.open();


    }
}